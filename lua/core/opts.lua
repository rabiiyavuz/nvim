local vim = vim
local opt = vim.opt
local cmd = vim.cmd

opt.laststatus = 3



vim.opt.number = true,
vim.opt.relativenumber = true,
numberwidth = 4,  
signcolumn = "yes",   


cmdheight = 1,  
opt.showcmd = true,


opt.background = "light"
opt.termguicolors = true


smartcase = true,
smartindent = true,
splitbelow = true,
splitright = true,


termguicolors = true, 



opt.foldenable = true
opt.foldmethod = 'expr'

opt.cursorline = true
opt.cursorlineopt = 'number'
opt.scrollopt = 'ver,hor,jump'
opt.virtualedit = 'block'


